module.exports = {
  block: 'page',
  title: 'Главная странциа',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      // Топ приборов в наличии;
      {block: 'mb-4', content: [
        {block: 'container', content: [
          {block: 'h', size: '1', content: [
            {block: 'a', content: 'Топ приборов в наличии'},
          ]},
        ]},
        {block: 'container', cls: 'container-xxl', content: [
          require('./common/swiper-row-card-product.bemjson.js'),
        ]},
      ]},
      // Рекламные вставки;
      {block: 'mb-4', content: [
        {block: 'container', content: [
          {block: 'row', cls: 'no-gutters', content: ((item) => new Array(2).fill(item))([
            {block: 'col-12', cls: 'col-md-6', content: [
              require('./common/card-teaser.bemjson'),
            ]},
          ])},
        ]},
      ]},
      // Последние поступившие предложения;
      {block: 'mb-4', content: [
        {block: 'container', content: [
          {block: 'h', size: '2', content: [
            {block: 'a', content: 'Последние поступившие предложения'},
          ]},
        ]},
        {block: 'container', cls: 'container-xxl', content: [
          require('./common/swiper-row-card-product.bemjson.js'),
        ]},
      ]},
        // Производители
      {block: 'mb-4', content: [
        {block: 'container', content: [
          {block: 'h', size: '2', content: [
            {block: 'a', content: 'Производители'},
          ]},
          {block: 'smart-overflow', content: [
            {elem: 'collapse', attrs: {id: 'test'}, content: [
              require('./common/manufacturer.bemjson.js'),
            ]},
            {elem: 'link', attrs: {'data-toggle': 'open', 'data-target': '#test'}, content: [
              {block: 'h', size: '4', cls: 'mb-0 text-dashed', content: [
                {tag: 'span', content: 'Показать все'},
                {tag: 'span', content: 'Свернуть'},
              ]},
            ]},
          ]},
        ]},
      ]},
      // Растяжка
      {block: 'mb-4', content: [
        {block: 'a', cls: 'd-block text-center', content: [
          {block: 'd-portrait-none', content: [
            {block: 'image', mods: {size: '1920x460'}},
          ]},
          {block: 'd-landscape-none', content: [
            {block: 'image', mods: {size: '1920x1920'}},
          ]},
        ]},
      ]},
      // Запросы на покупку
      {block: 'mb-4', content: [
        {block: 'container', content: [
          {block: 'h', size: '2', content: [
            {block: 'a', content: 'Запросы на покупку'},
          ]},
        ]},
        {block: 'container', cls: 'container-xxl', content: [
          require('./common/swiper-row-card-request.bemjson.js'),
        ]},
      ]},
      // О сервисе
      {block: 'mb-4', content: [
        {mix: {block: 'container'}, content: [
          {block: 'h', size: '2', content: [
            {block: 'a', content: 'О сервисе TradeInPribor'},
          ]},
          {tag: 'p', content: 'Данный сервис создан по инициативе группы Российских компаний в ответ на санкции в адрес России от стран- производителей высоко качественного измерительного и технологического оборудования, которые усложняют и запрещают поставку оборудования для Российских предприятий. Российские промышленные предприятия, имеющие в наличии измерительное и технологическое оборудование ведущих мировых производителей, могут подать бесплатные объявления о своем оборудовании или ознакомиться с имеющимися объявлениями для приобретения.'},
          {tag: 'p', content: 'Подать объявление может любой представитель Российского предприятия после регистрации и присвоения ему отдельного статуса. В объявлении можно указать оборудование не старше 15 лет с подробным описанием его состояния. Контактные данные представителя предприятия, подавшего объявление, могут быть предоставлены только представителю другого Российского предприятия, зарегистрированного на сайте со своим статусом. Представители коммерческих организаций не могут являться зарегистрированными лицами на данном сайте.'},
          {tag: 'p', content: 'Представители коммерческих организаций могут подать заявку администратору этого сервиса на размещение баннерной рекламы на платно основе. В общем доступе может быть доступна всем пользователям интернет ресурса только краткая информация об продукте в объявлении.'},
          {tag: 'p', content: 'Администрация сервиса не несет ответственности за недостоверную информацию размещенном в объявлении. Администрация в обязательном порядке осуществляет модерацию всех подаваемых объявлений в течение 24 часов с момента подачи заявки от представителя предприятия или коммерческой организации и имеет право отказать в активизации объявления или рекламы.'},
        ]},
      ]},
      // Новости
      {block: 'bg-light', cls: 'pt-3', content: [
        {block: 'container', content: [
          {block: 'h', size: '2', content: [
            {block: 'a', content: 'Новости'},
          ]},
          {block: 'row', content: ((item) => new Array(2).fill(item))([
            {block: 'col-12', cls: 'col-lg-6 pb-3', content: [
              require('./common/card-news.bemjson'),
            ]},
          ])},
        ]},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
