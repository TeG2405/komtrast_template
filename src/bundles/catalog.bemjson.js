module.exports = {
  block: 'page',
  title: 'Каталог - предложений',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      {cls: 'container container-xxl', content: [
        {block: 'h', size: '1', cls: 'd-xxl-none', content: 'Каталог приборов в наличии'},
        {cls: 'row align-items-start', content: [
          {elem: 'inner', cls: 'col-12 col-lg-8 col-xl-9 col-xxl-10 order-lg-1', content: [
            {block: 'h', size: '2', cls: 'h1 d-none d-xxl-block', content: 'Каталог приборов в наличии'},
            {block: 'catalog', content: [
              {block: 'row', content: new Array(35).fill([
                {block: 'col-12', cls: 'py-2 col-sm-6 col-md-4 col-xxl-10-2', content: [
                  require('./common/card-product.bemjson'),
                ]},
              ])},
            ]},
            {block: 'w-100 mt-3', content: [
              require('./common/pagination.bemjson'),
            ]},
          ]},
          {elem: 'sidebar', cls: 'col-12 col-lg-4 col-xl-3 col-xxl-2', content: [
            require('./common/filter.bemjson'),
            {block: 'mt-3', content: [
              {block: 'row', cls: 'no-gutters', content: ((item) => new Array(2).fill(item))([
                {block: 'col-12', cls: 'col-md-6 col-lg-12', content: [
                  require('./common/card-teaser-small.bemjson'),
                ]},
              ])},
            ]},
          ]},
        ]},
      ]},
      // Растяжка
      {block: 'mt-3', content: [
        {block: 'a', cls: 'd-block text-center', content: [
          {block: 'd-portrait-none', content: [
            {block: 'image', mods: {size: '1920x460'}},
          ]},
          {block: 'd-landscape-none', content: [
            {block: 'image', mods: {size: '1920x1920'}},
          ]},
        ]},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
