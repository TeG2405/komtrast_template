module.exports = {
  block: 'page',
  title: 'Каталог - детальная',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      {cls: 'container container-xxl', content: [
        {block: 'h', size: '1', cls: 'd-xxl-none', content: [
          {block: 'a', content: 'Каталог приборов'},
          {block: 'fi', mods: {icon: 'quotes-right'}, cls: 'text-primary text-large mx-1'},
          {tag: 'span', content: 'Rohde&Schwarz FSV13'},
        ]},
        {cls: 'row align-items-start', content: [
          {elem: 'inner', cls: 'col-12 col-lg-8 col-xl-9 col-xxl-10 order-lg-1', content: [
            {block: 'h', size: '2', cls: 'h1 d-none d-xxl-block', content: [
              {block: 'a', content: 'Каталог приборов'},
              {block: 'fi', mods: {icon: 'quotes-right'}, cls: 'text-primary text-large mx-1'},
              {tag: 'span', content: 'Rohde&Schwarz FSV13'},
            ]},
            {block: 'detail', content: [
              {elem: 'row', content: [
                {elem: 'col', elemMods: {pull: 'left'}, cls: 'order-2', content: [
                  require('./common/swiper-product.bemjson'),
                ]},
                {elem: 'col', elemMods: {pull: 'left'}, cls: 'order-6', content: [
                  require('./common/nav-product.bemjson'),
                ]},
                {elem: 'col', elemMods: {pull: 'left'}, cls: 'order-5', content: [
                  {tag: 'p', content: 'R&S®FSV – самый быстрый и наиболее универсальный анализатор спектра и сигналов, предназначенный для требовательных и бережливых пользователей, занятых разработкой, производством, установкой и обслуживанием радиотехнических систем.'},
                  {tag: 'p', content: 'Анализатор сигналов серии PXA является самым высокопроизводительным анализатором дальнейшим развитием существующих анализаторов сигналов с высокими рабочими характеристиками. Он охватывает диапазон частот до 50 ГГц и отличается'},
                  {tag: 'p', content: 'Он охватывает диапазон частот до 50 ГГц и отличается уникальнои гибкостью благодаря широкому спектру измерительных приложений и возможности модернизации аппаратнои части. Используя внешние смесители, можно расширить диапазон частот до 325 ГГц и выше.'},
                ]},
                {elem: 'col', elemMods: {pull: 'right'}, cls: 'order-1', content: [
                  {block: 'p-3 pb-xl-0 pt-xl-0', cls: 'bg-lighten bg-xl-transparent', content: [
                    require('./common/feature-large.bemjson'),
                  ]},
                ]},
                {elem: 'col', elemMods: {pull: 'right'}, cls: 'order-3', content: [
                  {block: 'px-3', content: [
                    {block: 'row', cls: 'align-items-center', content: [
                      {cls: 'col-12 col-sm-6 col-xl-12 col-xxl-6', content: [
                        {block: 'text-nowrap', cls: 'h1 my-0 py-2', content: '3 450 000 руб.'},
                      ]},
                      {cls: 'col-12 col-sm-6 pl-sm-0 col-xl-12 pl-xl-2 col-xxl-6 pl-xxl-0', content: [
                        {block: 'btn', cls: 'btn-outline-primary btn-lg btn-xl btn-block', content: 'В корзину'},
                      ]},
                    ]},
                  ]},
                ]},
                {elem: 'col', elemMods: {pull: 'right'}, cls: 'order-7', content: [
                  {block: 'px-xl-3', content: [
                    {block: 'row', content: [
                      {block: 'col-12', cls: 'col-xxl-6', content: [
                        {block: 'h', size: '5', cls: 'my-1 d-flex align-items-center', content: [
                          {block: 'fi', cls: 'align-middle mr-2 d-none d-xxl-inline', mods: {icon: 'comment'}},
                          {content: [
                            {tag: 'span', cls: 'align-middle', content: 'Не тот прибор который вам нужен?'},
                          ]},
                        ]},
                      ]},
                      {block: 'col-12', cls: 'col-xxl-5 pl-xxl-3', content: [
                        {block: 'h', size: '5', cls: 'my-1 d-flex align-items-center', content: [
                          {content: [
                            {block: 'a', cls: 'align-middle', content: 'Создайте запрос на покупку'},
                          ]},
                        ]},
                      ]},
                    ]},
                  ]},
                ]},
                {elem: 'col', elemMods: {pull: 'right'}, cls: 'order-4', content: [
                  {block: 'p-3', cls: 'bg-lighten', content: [
                    require('./common/feature-small.bemjson'),
                  ]},
                ]},
              ]},
            ]},
          ]},
          {elem: 'sidebar', cls: 'col-12 col-lg-4 col-xl-3 col-xxl-2', content: [
            {block: 'mt-3 mt-lg-0', content: [
              {block: 'row', cls: 'no-gutters', content: ((item) => new Array(2).fill(item))([
                {block: 'col-12', cls: 'col-md-6 col-lg-12', content: [
                  require('./common/card-teaser-small.bemjson'),
                ]},
              ])},
            ]},
          ]},
        ]},
      ]},
      // Растяжка
      {block: 'mt-3', content: [
        {block: 'a', cls: 'd-block text-center', content: [
          {block: 'd-portrait-none', content: [
            {block: 'image', mods: {size: '1920x460'}},
          ]},
          {block: 'd-landscape-none', content: [
            {block: 'image', mods: {size: '1920x1920'}},
          ]},
        ]},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
