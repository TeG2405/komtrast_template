module.exports = [
    {block: 'logo', cls: 'd-flex', content: [
        {elem: 'image', content: [
            {block: 'img', src: 'images/logo_header.svg'},
        ]},
        {elem: 'slogan', cls: 'd-none d-lg-block pt-xxl-2', content: 'Доска бесплатных объявлений для предприятий радиоэлектронной промышленности'},
    ]},
];
