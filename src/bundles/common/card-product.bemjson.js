module.exports = [
    {block: 'card-product', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '200x150'}},
        ]},
        {elem: 'label', content: 'В наличии в Челябинске'},
        {elem: 'title', cls: 'h4 my-0', content: 'Pendulum CNT-91R'},
        {elem: 'price', cls: 'h4 my-0', content: '1 000 000 руб.'},
        {elem: 'description', content: 'Год выпуска: 2000г.'},
    ]},
];
