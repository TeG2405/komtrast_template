module.exports = [
    {block: 'phone', content: [
        {block: 'a', cls: 'h3 my-0 unerline-none', href: 'tel:+79032513997', content: '+7 903 251-39-97'},
        {content: '— по вопросам работы сервиса'},
    ]},
];
