module.exports = [
    {block: 'navigation', content: [
        {elem: 'item', cls: 'h5 mt-0', content: [
            {block: 'a', content: 'Каталог приборов в наличии'},
        ]},
        {elem: 'item', cls: 'h5 mt-0', content: [
            {block: 'a', content: 'Подать запрос на покупку'},
        ]},
        {elem: 'item', cls: 'h5 mt-0', content: [
            {block: 'a', content: 'Продать прибор'},
        ]},
    ]},
];
