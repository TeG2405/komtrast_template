const NAMES = Array.from({length: 20}, () => ['Fluke', 'Tektronix Rohde', 'Schwarz', 'TPT', 'Pendulum'][Math.floor(Math.random() * 4)]);
module.exports = [
    {block: 'manufacturer', content: NAMES.map((name)=>{
        return [
            {elem: 'li', content: [
                {elem: 'link', cls: 'h4 m-0', content: [
                    {block: 'a', cls: 'pr-1', content: name},
                ]},
            ]},
        ];
    })},
];
