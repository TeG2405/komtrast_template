module.exports = [
    {block: 'logo', cls: 'd-flex', content: [
        {elem: 'image', content: [
            {block: 'img', cls: 'bg-primary border-primary', src: 'images/logo_footer.svg'},
        ]},
        {elem: 'slogan', cls: 'd-none d-sm-block d-xl-none text-white', content: 'Доска бесплатных объявлений для предприятий радиоэлектронной промышленности'},
    ]},
];
