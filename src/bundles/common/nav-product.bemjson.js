const ITEMS = [
    {icon: 'guide', title: 'Инструкция по применению'},
    {icon: 'certificate', title: 'Сертификат проверки'},
    {icon: 'like', title: 'Стандарт применения'},
];
module.exports = ITEMS.map((item) => [
    {block: 'h', size: '5', cls: 'my-2 d-flex align-items-center', content: [
        {block: 'fi', cls: 'mr-3', mods: {icon: item.icon}},
        {content: [
            {block: 'a', content: item.title},
        ]},
    ]},
]);

