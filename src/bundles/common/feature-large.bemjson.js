const ITEMS = [
    {label: 'Производитель прибора', value: 'Keysight'},
    {label: 'Марка прибора:', value: 'N9030A'},
    {label: 'Краткое описание прибора:', value: 'Анализатор спектра, диапазон от 3 Гц до 26,5 ГГц, полоса анализа 25 МГц, опция 526'},
    {label: 'Место нахождения:', value: 'г. Санкт Петербург '},
    {label: 'Дата производства:', value: 'август 2016 года'},
];
module.exports = [
    {block: 'feature', cls: 'text-xxl-large', content: ITEMS.map((item)=>[
        {elem: 'row', cls: 'row mb-xxl-1', content: [
            {elem: 'label', cls: 'col-12 col-sm-6', content: item.label},
            {elem: 'value', cls: 'col-12 col-sm-6 pl-sm-0', content: item.value},
        ]},
    ])},
];
