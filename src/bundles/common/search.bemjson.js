module.exports = [
    {block: 'search', content: [
        {elem: 'input', cls: 'form-control border-white', placeholder: 'Найти во всех регионах'},
        {elem: 'control', content: [
            {block: 'fi', mods: {icon: 'search'}},
        ]},
    ]},
];
