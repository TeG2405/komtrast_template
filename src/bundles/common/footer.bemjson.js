module.exports = [
  {block: 'footer', content: [
    {elem: 'container', cls: 'container container-xxl d-none d-xxl-block', content: [
      {elem: 'row', cls: 'row align-items-center justify-content-between', content: [
        {block: 'col', cls: 'col-sm-6 col-lg-8 col-xl-6 col-xxl-2', content: [
          require('./logo_footer.bemjson'),
        ]},
        {block: 'col', cls: 'col-sm-6 col-lg-4 col-xl-3 col-xxl-2', content: [
          {block: 'developer', cls: 'text-sm-right', content: [
            '<a href="#">Разработка сайта</a> — ',
            {block: 'fi', mods: {icon: 'iv'}, cls: 'text-primary'},
          ]},
        ]},
      ]},
    ]},
    {elem: 'container', cls: 'container', content: [
      {block: 'row', cls: 'align-items-center', content: [
        {block: 'col-12', cls: 'order-xxl-1 py-1 col-lg-8 col-xl-3 d-xxl-none', content: [
          require('./logo_footer.bemjson'),
        ]},
        {block: 'col-12', cls: 'order-xxl-5 py-1 col-xl-9', content: [
          {block: 'nav', mods: {'inline': true}, content: [
            'Реклама на сайте',
            'Каталог приборов',
            'Продать прибор',
            'Подать запрос на покупку',
            {elem: 'item', cls: 'dropdown', content: [
              {elem: 'link', attrs: {'data-toggle': 'dropdown', 'role': 'button'}, content: 'Ещё'},
              {block: 'dropdown-menu', mods: {'size': 'sm'}, content: [
                'Помощь',
                'Задать вопрос',
                'Реклама на сайте',
              ]},
            ]},
          ]},
        ]},
        {block: 'col-12', cls: 'order-xxl-1 py-1 col-xl-3 d-none d-xl-block', content: [
          {block: 'logo', content: [
            {elem: 'slogan', cls: 'text-white', content: 'Доска бесплатных объявлений для предприятий радиоэлектронной промышленности'},
          ]},
        ]},
        {block: 'col-12', cls: 'order-xxl-2 py-1 col-sm-12 col-xl-3 col-xxl-3', content: [
          {block: 'phone', content: [
            {block: 'a', cls: 'h4 my-0 unerline-none text-white', href: 'tel:+79032513997', content: '+7 903 251-39-97'},
            {elem: 'small', content: '— по вопросам работы сервиса'},
          ]},
        ]},
        {block: 'col-12', cls: 'order-xxl-3 py-1 col-sm-12 col-xl col-xxl-5', content: [
          {block: 'a', cls: 'h5 my-0 unerline-none text-primary', href: 'mailto:info@tradeinpribor.ru', content: 'Info@tradeinpribor.ru'},
        ]},
        {block: 'col-12', cls: 'order-xxl-4 py-1 col-sm-6 col-xl col-xxl-3 text-xl-center text-xxl-left', content: [
          '© TiP 2018',
        ]},
        {block: 'col-12', cls: 'order-xxl-1 py-1 col-sm-6 col-xl-2 d-xxl-none', content: [
          {block: 'developer', cls: 'text-sm-right', content: [
            '<a href="#">Разработка сайта</a> — ',
            {block: 'fi', mods: {icon: 'iv'}, cls: 'text-primary'},
          ]},
        ]},
      ]},
    ]},
  ]},
];
