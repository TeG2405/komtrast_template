module.exports = [
    {block: 'toolbar', content: [
        {block: 'nav', mods: {'inline': true}, content: [
            {elem: 'item', cls: 'dropdown d-lg-none', content: [
                {elem: 'link', attrs: {'data-toggle': 'dropdown', 'role': 'button'}, content: 'О сервисе'},
                {block: 'dropdown-menu', mods: {'size': 'sm'}, content: [
                    'Помощь',
                    'Задать вопрос',
                    'Реклама на сайте',
                    {elem: 'divider', cls: 'my-1 d-md-none'},
                    {elem: 'text', cls: 'text-nowrap  d-md-none', content: [
                        {block: 'a', cls: 'h6 my-0 unerline-none', href: 'tel:+79032513997', content: '+7 903 251-39-97'},
                        {content: '— по вопросам работы сервиса'},
                    ]},
                ]},
            ]},
            {elem: 'item', cls: 'd-none d-lg-block', content: 'Помощь'},
            {elem: 'item', cls: 'd-none d-lg-block', content: 'Задать вопрос'},
            {elem: 'item', cls: 'd-none d-lg-block', content: 'Реклама на сайте'},
        ]},
        {block: 'list', cls: 'list-inline mb-0 d-none d-md-block d-xl-none d-xxl-block', content: [
            {elem: 'li', cls: 'list-inline-item', content: {block: 'a', cls: 'h6 my-0 unerline-none', href: 'tel:+79032513997', content: '+7 903 251-39-97'}},
            {elem: 'li', cls: 'list-inline-item', content: '— по вопросам работы сервиса'},
        ]},
        {block: 'nav', mods: {'inline': true}, content: [
            'Регистрация',
            'Вход',
        ]},
    ]},
];
