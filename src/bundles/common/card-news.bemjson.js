module.exports = [
    {block: 'card-news', content: [
        {elem: 'inner', content: [
            {elem: 'image', cls: 'd-none d-xl-block', content: [
                {block: 'image', mods: {size: '160x160'}},
            ]},
            {elem: 'caption', content: [
                {elem: 'date', content: '28.02.2018'},
                {elem: 'title', cls: 'h5 mt-0', content: [
                    {block: 'a', content: 'На Урале создали инновационную технологию для предприятий черной металлургии.'},
                ]},
                {elem: 'description', content: [
                    {block: 'image', cls: 'd-xl-none', mods: {size: '160x160'}},
                    'Представители Уральской горно-металлургической компании сообщили, что разработчики Технического университета при организации создали инновационную технологию, которая перевернуло стандартное понимание добычи цинкового порошка из пылей сталеплавильных производств.'
                ]},
            ]},
        ]},
    ]},
];
