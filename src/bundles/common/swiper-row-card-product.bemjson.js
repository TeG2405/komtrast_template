module.exports = [
    {block: 'row', content: [
        {block: 'swiper-row', cls: 'swiper-container swiper-container-horizontal w-100', content: [
            {block: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                {block: 'col-12', cls: 'swiper-slide col-sm-6 col-lg-4 col-xl-3 col-xxl-2', content: require('./card-product.bemjson')},
            ])},
            {elem: 'button', mods: {prev: true}, content: {block: 'fi', mods: {icon: 'angle-left'}}},
            {elem: 'button', mods: {next: true}, content: {block: 'fi', mods: {icon: 'angle-right'}}},
        ]},
    ]},
];
