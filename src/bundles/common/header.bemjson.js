module.exports = [
  {block: 'header', content: [
    {elem: 'border', content: [
      {block: 'container', content: [
        require('./toolbar.bemjson'),
      ]},
    ]},
    {elem: 'container', cls: 'container container-xxl pt-3 pt-xxl-0', content: [
      {elem: 'row', cls: 'row justify-content-between', content: [
        {block: 'col', cls: 'col-sm-6 col-lg-8 col-xl-6 col-xxl-2', content: [
          require('./logo_header.bemjson'),
        ]},
        {block: 'col', cls: 'd-none d-xl-block d-xxl-none col-xl-3', content: [
          require('./phone_header.bemjson'),
        ]},
        {block: 'col', cls: 'col-sm-6 col-lg-4 col-xl-3 col-xxl-2', content: [
          require('./basket-widget.bemjson'),
        ]},
      ]},
    ]},
    {elem: 'container', cls: 'container', content: [
      {block: 'py-3', content: [
        require('./navigation.bemjson'),
      ]},
      {block: 'pb-3', content: [
        require('./search.bemjson'),
      ]},
    ]},
  ]},
];
