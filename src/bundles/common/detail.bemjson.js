module.exports = [
    {block: 'detail', content: [
        {cls: 'row', content: [
            {cls: 'col-12 col-md-6 col-lg-5', content: [
                {block: 'row', content: [
                    {block: 'col-12', content: [
                        require('./common/swiper-product.bemjson'),
                    ]},
                    {block: 'col-12', content: [
                        require('./common/nav-product.bemjson'),
                    ]},
                    {block: 'col-12', content: [
                        {tag: 'p', content: 'R&S®FSV – самый быстрый и наиболее универсальный анализатор спектра и сигналов, предназначенный для требовательных и бережливых пользователей, занятых разработкой, производством, установкой и обслуживанием радиотехнических систем.'},
                        {tag: 'p', content: 'Анализатор сигналов серии PXA является самым высокопроизводительным анализатором дальнейшим развитием существующих анализаторов сигналов с высокими рабочими характеристиками. Он охватывает диапазон частот до 50 ГГц и отличается'},
                        {tag: 'p', content: 'Он охватывает диапазон частот до 50 ГГц и отличается уникальнои гибкостью благодаря широкому спектру измерительных приложений и возможности модернизации аппаратнои части. Используя внешние смесители, можно расширить диапазон частот до 325 ГГц и выше.'},
                    ]},
                ]},
            ]},
            {cls: 'col-12 col-md-6 col-lg-7', content: [
                {block: 'p-3 pt-xl-0', content: [
                    require('./common/feature-large.bemjson'),
                ]},
                {block: 'p-3', content: [
                    {block: 'row', cls: 'align-items-center', content: [
                        {cls: 'col-12 col-xl-6', content: [
                            {block: 'text-nowrap', cls: 'h1 my-0', content: '3 450 000 руб.'},
                        ]},
                        {cls: 'col-12 col-xl-6 pl-xl-0', content: [
                            {block: 'btn', cls: 'btn-outline-primary btn-lg btn-xl btn-block', content: 'В корзину'},
                        ]},
                    ]},
                ]},
                {block: 'p-3', content: [
                    {block: 'row', content: [
                        {block: 'col-12', cls: 'col-xxl-7', content: [
                            {block: 'h', size: '5', cls: 'my-1 d-flex align-items-center', content: [
                                {block: 'fi', cls: 'align-middle mr-2 d-none d-xxl-inline', mods: {icon: 'comment'}},
                                {content: [
                                    {block: 'a', cls: 'align-middle', content: 'Не тот прибор который вам нужен?'},
                                ]},
                            ]},
                        ]},
                        {block: 'col-12', cls: 'col-xxl-5 pl-xxl-0', content: [
                            {block: 'h', size: '5', cls: 'my-1 d-flex align-items-center', content: [
                                {content: [
                                    {block: 'a', cls: 'align-middle', content: 'Создайте запрос на покупку'},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
                {block: 'p-3', cls: 'bg-lighten', content: [
                    require('./common/feature-small.bemjson'),
                ]},
            ]},
        ]},
    ]},
];
