const LENGTH = 10;
module.exports = [
    {block: 'swiper-product', content: [
        {elem: 'label', cls: 'text-danger', content: [
            {block: 'fi', mods: {icon: 'new'}},
        ]},
        {cls: 'swiper-container swiper-container-horizontal w-100', content: [
            {block: 'swiper-wrapper', content: new Array(LENGTH).fill([
                {block: 'swiper-slide', content: {block: 'image', mods: {size: '690x620'}}},
            ])},
        ]},
        {elem: 'row', cls: 'mt-2', content: [
            {cls: 'swiper-container swiper-container-horizontal fade', content: [
                {block: 'swiper-wrapper', content: [
                    {block: 'swiper-slide', cls: 'w-auto selected', content: {block: 'image', mods: {size: '60x60'}}},
                    new Array(LENGTH - 1).fill([
                        {block: 'swiper-slide', cls: 'w-auto', content: {block: 'image', mods: {size: '60x60'}}},
                    ]),
                ]},
                {elem: 'button', mods: {prev: true}, content: {block: 'fi', mods: {icon: 'angle-left'}}},
                {elem: 'button', mods: {next: true}, content: {block: 'fi', mods: {icon: 'angle-right'}}},
            ]},
        ]},
    ]},
];
