module.exports = [
    {block: 'filter', attrs: {id: 'CATALOG_FILTER'}, content: [
        {elem: 'label', attrs: {'data-toggle': 'open', 'data-target': '#CATALOG_FILTER'}, content: [
            {block: 'fi', cls: 'align-middle', mods: {icon: 'filter'}},
        ]},
        {elem: 'inner', content: [
            {elem: 'item', content: [
                {elem: 'title', content: 'Тип прибора'},
                {elem: 'panel', content: new Array(10).fill([
                    {block: 'form-check', content: 'Значение характеристики'},
                ])},
            ]},
            {elem: 'item', content: [
                {elem: 'title', content: 'Производитель'},
                {elem: 'panel', content: new Array(10).fill([
                    {block: 'form-check', content: 'Значение характеристики'},
                ])},
            ]},
            {elem: 'item', content: [
                {elem: 'title', content: 'Цена'},
                {elem: 'panel', content: [
                    {block: 'range', range: [0, 1500]},
                ]},
            ]},
            {elem: 'item', content: [
                {elem: 'title', content: 'Точность'},
                {elem: 'panel', content: new Array(10).fill([
                    {block: 'form-check', name: 'RADIO', type: 'radio', content: 'Значение характеристики'},
                ])},
            ]},
            {elem: 'item', content: [
                {elem: 'panel', content: [
                    {elem: 'control', content: [
                        {block: 'fi', cls: 'align-middle', mods: {icon: 'times'}},
                        {tag: 'span', cls: 'align-middle', content: 'Сбросить всё'},
                    ]},
                ]},
            ]},
        ]},
        {cls: 'd-none', content: [
            {elem: 'popover', content: [
                {block: 'btn', cls: 'btn-primary btn-sm btn-block', content: 'Выбранно (10) показать'},
            ]},
        ]},
    ]},
];

