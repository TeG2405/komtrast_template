module.exports = [
    {block: 'card-teaser', content: [
        {elem: 'inner', content: [
            {elem: 'image', content: [
                {block: 'image', mods: {size: '200x150'}},
            ]},
            {elem: 'caption', content: [
                {elem: 'title', cls: 'h4', content: 'АО «Клинкман СПб» представляет новый контроллер Unitronics UniStream'},
            ]},
        ]},
    ]},
];
