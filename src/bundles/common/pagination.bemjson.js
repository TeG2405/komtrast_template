module.exports = [
    {block: 'pagination-fluid', attrs: {'data-active': 25}, cls: 'swiper-container swiper-container-horizontal', tag: 'nav', content: [
        {elem: 'control', mods: {prev: true}, content: [
            {cls: 'page-link', content: [
                {block: 'fi', mods: {icon: 'angle-left'}},
            ]},
        ]},
        {block: 'pagination', cls: 'swiper-wrapper mb-0 w-auto', length: 50, active: 25},
        {elem: 'control', mods: {next: true}, content: [
            {cls: 'page-link', content: [
                {block: 'fi', mods: {icon: 'angle-right'}},
            ]},
        ]},
    ]},
];
