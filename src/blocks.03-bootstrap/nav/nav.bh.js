module.exports = function(bh) {
    bh.match('nav', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'item', content: item} : item;
            }),
            true
        );
    });
};
