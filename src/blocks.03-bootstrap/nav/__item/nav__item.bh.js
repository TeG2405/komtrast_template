module.exports = function(bh) {
    bh.match('nav__item', function(ctx, json) {
        ctx
            .tag('li')
            .mix({block: 'nav-item'})
            .content({elem: 'link', content: ctx.content()}, ctx.isSimple(ctx.content()));
    });
};
