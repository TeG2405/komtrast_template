module.exports = function(bh) {
    bh.match('dropdown-menu__item', function(ctx, json) {
        ctx
            .tag('a')
            .attr('href', '#')
            .mix({block: 'dropdown-item'});
    });
};
