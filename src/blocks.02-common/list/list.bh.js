module.exports = function(bh) {
    bh.match('list', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'li', content: item} : item
            }),
            true
        );
    });
};
