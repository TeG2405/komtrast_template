const EqHeight = (($)=>{
    class EqHeight {
        constructor(selector) {
            this.elems = $(selector);
            this.points = null;
        }
        update() {
            this.points = [];
            this.elems.each((index, elem)=>{
                let offset = EqHeight.offset(elem).top;
                if (this.points.indexOf(offset) < 0) {
                    this.points.push(offset);
                }
            });
            this.points
                .map((point)=>{
                    return Array.prototype.filter.call(this.elems, (elem)=>{
                        return EqHeight.offset(elem).top == point;
                    });
                })
                .map((row)=>{
                    let height = Math.max.apply(null, row.map((elem)=>{
                        elem.style.height = '';
                        return elem.offsetHeight;
                    }));
                    if (height) {
                        row.forEach((elem)=>{
                            elem.classList.add('eq-controlled');
                            if (elem.offsetHeight !== height) {
                                elem.style.height = height + 'px';
                            }
                        });
                    }
                });
        }
        static offset(elem) {
            if (!elem.getClientRects().length) {
                return {top: 0, left: 0};
            }
            let rect = elem.getBoundingClientRect();
            let win = elem.ownerDocument.defaultView;
            return {
                top: rect.top + win.pageYOffset,
                left: rect.left + win.pageXOffset,
            };
        }
    }
    // Дефолтная инициализация;
    var card = new EqHeight('.card-product__title');
    var refresh = () => {
        card.update();
    };
    refresh();
    $(document)
        .on('ready', refresh)
        .on('OnDocumentHtmlChanged', refresh)
        .on('shown.bs.modal', '.modal', refresh)
        .on('shown.bs.tab', '[data-toggle="tab"]', refresh);
    $(window).on('load resize', refresh);
    return EqHeight;
})($);
export default EqHeight;
