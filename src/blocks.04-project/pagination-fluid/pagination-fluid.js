import Swiper from 'swiper/dist/js/swiper.min';
(()=>{
    const init = () => {
        Array.prototype.forEach.call(document.getElementsByClassName('pagination-fluid'), (elem) => {
            if (!elem.swiper) {
                elem.swiper = new Swiper(elem, {
                    init: false,
                    slidesPerView: 'auto',
                    centeredSlides: true,
                    watchOverflow: true,
                    watchSlidesVisibility: true,
                    spaceBetween: 0,
                    initialSlide: Number(elem.dataset.active) - 1,
                    navigation: {
                        nextEl: '.pagination-fluid__control_next',
                        prevEl: '.pagination-fluid__control_prev',
                    },
                });
                if (elem.swiper.wrapperEl.offsetWidth + 80 > elem.swiper.el.offsetWidth) {
                    elem.swiper.wrapperEl.classList.remove('w-auto');
                    elem.swiper.init();
                    let visible = elem.swiper.el.getElementsByClassName('swiper-slide-visible').length;
                    elem.swiper.navigation.nextEl.addEventListener('click', (event)=>{
                        elem.swiper.slideTo(elem.swiper.activeIndex + Math.floor(visible/2));
                        event.stopPropagation();
                        event.preventDefault();
                    });
                    elem.swiper.navigation.prevEl.addEventListener('click', (event)=>{
                        elem.swiper.slideTo(elem.swiper.activeIndex + Math.floor(visible/-2));
                        event.stopPropagation();
                        event.preventDefault();
                    });
                } else elem.swiper.wrapperEl.classList.add('w-auto');
            }
        });
    };
    init();
    document.addEventListener('OnDocumentHtmlChanged', init);
})();
