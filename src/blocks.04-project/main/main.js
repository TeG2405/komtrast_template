const Sidebar = (($)=>{
    const OFFSET_TOP = 0;
    class Sidebar {
        constructor(className) {
            this.elems = document.getElementsByClassName(className);
            this.pageYOffset = window.pageYOffset;
            window.addEventListener('scroll', this.check.bind(this));
        }
        check(event) {
            let pageYOffsetOld = this.pageYOffset;
            Array.prototype.forEach.call(this.elems, (elem) => {
                let offsetTopWrapper = Sidebar.offset(elem.parentNode).top;
                let speed = elem.parentElement.offsetHeight / elem.scrollHeight;
                if (speed <= 1) {
                    elem.classList.add('position-relative');
                } else {
                    elem.classList.remove('position-relative');
                    if (offsetTopWrapper < window.pageYOffset + OFFSET_TOP) {
                        if (offsetTopWrapper + elem.parentNode.offsetHeight > window.pageYOffset + window.innerHeight) {
                            elem.scrollTop = elem.scrollTop + (window.pageYOffset - pageYOffsetOld) * speed;
                        } else {
                            elem.scrollTop = elem.parentNode.offsetHeight;
                        }
                    } else {
                        elem.scrollTop = 0;
                    }
                }
            });
            this.pageYOffset = window.pageYOffset;
        }
        static offset(elem) {
            if (!elem.getClientRects().length) {
                return {top: 0, left: 0};
            }
            let rect = elem.getBoundingClientRect();
            let win = elem.ownerDocument.defaultView;
            return {
                top: rect.top + win.pageYOffset,
                left: rect.left + win.pageXOffset,
            };
        }
    }
    new Sidebar('main__sidebar');
    return Sidebar;
})($);
export default Sidebar;
