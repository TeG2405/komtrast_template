module.exports = function(bh) {
    bh.match('filter__title', function(ctx, json) {
        ctx.tag('a').cls('collapsed').attrs({
            'data-toggle': 'collapse',
            'href': '#'+ctx.tParam('ID'),
        });
    });
};
