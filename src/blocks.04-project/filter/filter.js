import {Modal, Util} from 'bootstrap';
(($) => {
    const modalApi = new Modal();
    const EVENT = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
        CHANGE: 'change',
        CLICK: 'click',
    };
    const CLASS = {
        SHOW: 'show',
        OPEN: 'modal-open',
        BACKDROP: 'modal-backdrop',
        FADE: 'fade',
    };
    const SELECTOR = {
        BLOCK: '.filter',
        UPDATE_CONTROL: '.filter__popover .btn',
    };
    $(document)
        .on(EVENT.SHOW, SELECTOR.BLOCK, (event)=>{
            let $target = $(event.target);
            modalApi._checkScrollbar();
            modalApi._setScrollbar();
            $(document.body).addClass(CLASS.OPEN);
            if (!modalApi._backdrop) {
                modalApi._backdrop = document.createElement('div');
                modalApi._backdrop.classList.add(CLASS.BACKDROP);
                modalApi._backdrop.classList.add(CLASS.FADE);
                $(modalApi._backdrop).appendTo(document.body);
                Util.reflow(modalApi._backdrop);
                $(modalApi._backdrop).one('click', () => {
                    $target
                        .removeClass(CLASS.SHOW)
                        .trigger(new $.Event(EVENT.HIDE));
                });
            }
            modalApi._backdrop.classList.add(CLASS.SHOW);
        })
        .on(EVENT.HIDE, SELECTOR.BLOCK, (event)=>{
            let $target = $(event.target);
            if ($target.is(SELECTOR.BLOCK) && modalApi._backdrop) {
                $(document.body).removeClass(CLASS.OPEN);
                modalApi._resetScrollbar();
                modalApi._backdrop.classList.remove(CLASS.SHOW);
                $(modalApi._backdrop)
                    .one(Util.TRANSITION_END, () => {
                        modalApi._removeBackdrop();
                    })
                    .emulateTransitionEnd(Util.getTransitionDurationFromElement(modalApi._backdrop));
            }
        })
        .on(EVENT.CHANGE, SELECTOR.BLOCK, (event) => {
            $(event.target)
                .closest('.filter__item')
                .append($(event.currentTarget)
                    .find('.filter__popover'));
        })
        .on(EVENT.CLICK, SELECTOR.UPDATE_CONTROL, (event) => {
            let backdrop = document.createElement('div');
            backdrop.classList.add('progress-updated');
            backdrop.classList.add('fade');
            $('.catalog').append(backdrop);
            Util.reflow(backdrop);
            backdrop.classList.add('show');
            setTimeout(() => {
                backdrop.classList.remove('show');
                $(backdrop)
                    .one(Util.TRANSITION_END, () => {
                        backdrop.remove();
                    })
                    .emulateTransitionEnd(Util.getTransitionDurationFromElement(backdrop));
            }, 1500);
            event.preventDefault();
        });
})($);


