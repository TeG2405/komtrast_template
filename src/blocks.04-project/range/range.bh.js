module.exports = function(bh) {
    bh.match('range', function(ctx, json) {
        let minID = ctx.generateId();
        let maxID = ctx.generateId();
        ctx.content([
            {cls: 'd-flex mb-2 row', content: [
                {cls: 'pr-1 col-6', content: [
                    {elem: 'control', cls: 'form-control form-control-sm', content: [
                        'От:',
                        {elem: 'input', attrs: {name: minID, id: minID, placeholder: json.range[0]}, content: json.range[0]},
                    ]},
                ]},
                {cls: 'pl-1 col-6', content: [
                    {elem: 'control', cls: 'form-control  form-control-sm', content: [
                        'До:',
                        {elem: 'input', attrs: {name: maxID, id: maxID, placeholder: json.range[1]}, content: json.range[1]},
                    ]},
                ]},
            ]},
            {cls: 'py-1', content: [
                {elem: 'inner', attrs: {
                    'data-min': json.range[0],
                    'data-max': json.range[1],
                    'data-target-min': minID,
                    'data-target-max': maxID,
                }},
            ]},
        ]);
    });
};
