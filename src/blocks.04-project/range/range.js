import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/keycode';
import 'jquery-ui/ui/widgets/mouse';
import 'jquery-ui/ui/widgets/slider';
import 'jquery-ui-touch-punch';
(($)=>{
    const init = () => {
        $('.range').each((index, item) => {
            let $block = $(item);
            let $input = $block.find('input');
            let $inner = $block.find('.range__inner');
            $inner.slider({
                range: true,
                min: Number($inner.data('min')),
                max: Number($inner.data('max')),
                values: [
                    Number($input.get(0).value) ? Number($input.get(0).value) : Number($input.get(0).getAttribute('placeholder')),
                    Number($input.get(1).value) ? Number($input.get(1).value) : Number($input.get(1).getAttribute('placeholder')),
                ],
                slide: (event, ui) => {
                    $input.get(0).value = ui.values[0];
                    $input.get(1).value = ui.values[1];
                },
                stop: function(event, ui) {
                    $input.trigger('change');
                },
            });
            $input.on('keyup', (event)=>{
                if (event.target.value < Number($inner.data('min'))) {
                    event.target.value = Number(event.target.placeholder);
                }
                if (event.target.value > Number($inner.data('max'))) {
                    event.target.value = Number(event.target.placeholder);
                }
                if (Number(event.target.value)) {
                    event.target.value = Number(event.target.value);
                } else {
                    event.target.value = 0;
                }
                $inner.slider('values', [
                    Number($input.get(0).value) ? Number($input.get(0).value) : Number($input.get(0).getAttribute('placeholder')),
                    Number($input.get(1).value) ? Number($input.get(1).value) : Number($input.get(1).getAttribute('placeholder')),
                ]);
            });
        });
    };
    init();
    document.addEventListener('OnDocumentHtmlChanged', init);
})($);
