module.exports = function(bh) {
    bh.match('card-teaser', function(ctx, json) {
        ctx
            .tag('a')
            .attr('href', '#');
    });
};
