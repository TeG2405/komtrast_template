module.exports = function(bh) {
    bh.match('basket-widget', function(ctx, json) {
        ctx
            .tag('a')
            .attrs({
                'href': '#',
                'data-count': Math.floor(Math.random() * 100),
            });
    });
};
