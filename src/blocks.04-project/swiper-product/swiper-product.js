import Swiper from 'swiper/dist/js/swiper.min';
(($) => {
    Array.prototype.forEach.call(document.getElementsByClassName('swiper-product'), (elem) => {
        let main = elem.getElementsByClassName('swiper-container')[0];
        let thumbnail = elem.getElementsByClassName('swiper-container')[1];
        let apiThumbnail = new Swiper(thumbnail, {
            slidesPerView: 'auto',
            spaceBetween: 15,
            watchSlidesVisibility: true,
            initialSlide: Math.floor((thumbnail.offsetWidth/60)/2) - 1,
            centeredSlides: true,
            watchOverflow: true,
            navigation: {
                nextEl: elem.getElementsByClassName('swiper-product__button_next'),
                prevEl: elem.getElementsByClassName('swiper-product__button_prev'),
            },
            on: {
                init: function() {
                    thumbnail.classList.add('show');
                },
            },
        });
        let apiMain = new Swiper(main, {
            slidesPerView: 1,
            spaceBetween: 0,
            on: {
                slideChangeTransitionEnd: () => {
                    thumbnail.getElementsByClassName('selected')[0].classList.remove('selected');
                    thumbnail.getElementsByClassName('swiper-slide')[apiMain.snapIndex].classList.add('selected');
                },
            },
        });
        $(thumbnail).on('click', '.swiper-slide', (e) => {
            apiMain.slideTo(Array.from(thumbnail.getElementsByClassName('swiper-slide')).indexOf(e.currentTarget));
        });
    });
})($);
