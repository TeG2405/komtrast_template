module.exports = function(bh) {
    bh.match('search__input', function(ctx, json) {
        ctx.tag('input').attrs({
            type: 'text',
            value: ctx.content(),
            placeholder: json.placeholder
        }).content(false, true);
    });
};
